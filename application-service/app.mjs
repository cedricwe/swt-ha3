import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import MongoClient from "mongodb";


const app = express()
app.use(cors())
app.options('*', cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

let db;

MongoClient.connect('mongodb://mongodb:27017/applications', (err, client) => {
    if (err) throw err
    db = client.db('applications')
});

//generic get controller for a resource with the id
app.get('/:resource/:id', async (req, res) => {
     await db
        .collection(req.params.resource)
        .findOne({ _id: MongoClient.ObjectID(req.params.id) }, function (err, result) {
            res.json(result);
        });
});

//generic get controller for all resources of the "resource"-collection
app.get('/:resource', async (req, res) => {
    const snapshot = await db
        .collection(req.params.resource)
        .find()
        .toArray((err, result) => {
            res.status(200).json(result)
        });
})


//edit a resource with the id
app.put('/:resource/:id', async (req, res) => {
    const body = req.body
    console.log(body);
    delete body._id;
    await db
        .collection(req.params.resource)
        .replaceOne({ _id: MongoClient.ObjectID(req.params.id) }, body);

    res.status(200).send({ success: true, message: 'resource updated' })
})

//delete a resource with the id
app.delete('/:resource/:id', async (req, res) => {
    await db
        .collection(req.params.resource)
        .deleteOne({ _id: MongoClient.ObjectID(req.params.id) })
        .catch((err) => functions.logger.log(err))

    res.status(200).send({ success: true, message: 'resource deleted' })
})

//generic add controller for a resource
app.post('/:resource', async (req, res) => {
    const resource = req.body
    const addedResource = await db
        .collection(req.params.resource)
        .insertOne(resource, (err, saved) => {
            res.status(201).json(saved.ops[0])
        });
});

app.listen(3000, () => {
    console.log(`Application-Service listening on internal port 3000`)
})