import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import fileUpload from 'express-fileupload';
import fs from 'fs';

const app = express();
const PORT = 3000;

app.use(cors())
app.options('*', cors())
app.use(bodyParser.json())
app.use(fileUpload());

app.post('/files', (req, res) => {
    console.log(req.files);
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }
    const file = req.files.file;
    const fileName = file.name;
    file.mv('binaries/' + fileName, (err) => {
        if (err)
            return res.status(500).send(err);
        res.json({ 'filename': fileName });
    });
});

app.get('/files/:name', (req, res) => {
    console.log("HI");
    let file = fs.readFileSync('binaries/' + req.params.name);
    // res.contentType('image/jpeg');
    res.send(file);
});

app.listen(PORT, () => {
    console.log(`File-Service listening on internal port ${PORT}`)
})