# mail-service

## Usage:
```bash
curl --location --request POST 'http://localhost:3000/mail' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=1402313D0E99A49BF47BC3F183DE29CD' \
--data-raw '{
    "email":"cw@picapartner.com",
    "subject":"Test Email",
    "content":"This is a test mail"
}'
```