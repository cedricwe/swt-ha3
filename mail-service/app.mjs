import express from'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import nodemailer from 'nodemailer';

const app = express() // start the express server
const PORT = 3000;

app.use(cors())
app.options('*', cors())
app.use(bodyParser.json())

// The host, the protocol and also the login data are stored in the transporter
let transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'yasinmizrakuni@gmail.com',
        pass: 'akat123suki!',
    },
});

app.get('/', (req, res)=> {
    res.json({message: 'Mail-Service'});
});

//send a mail
app.post('/mail', async (req, res) => {
    await transporter.sendMail({
        from: '"Universität zu Köln" <yasinmizrakuni@gmail.com>', // sender address
        to: req.body.email, // list of receivers
        subject: `${req.body.subject}`, // Subject line
        html: `${req.body.content}`, // html body
    })

    res.send({ success: true })
})

app.listen(PORT, () => {
    console.log(`Mailservice listening on internal port ${PORT}`)
  })