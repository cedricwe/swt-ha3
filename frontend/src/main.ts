import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Toasted from 'vue-toasted';
import Multiselect from 'vue-multiselect'
import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);

Vue.component('multiselect', Multiselect)

Vue.use(Toasted, {
  duration: 2000,
  theme: 'bubble'
})


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
