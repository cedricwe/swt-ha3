import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Apply from '../views/applicant/Apply.vue';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: Apply
  },
  {
    path: '/applicant/referral',
    component: () => import('../views/applicant/Referral.vue')
  },
  {
    path: '/applicant/referral/received',
    component: () => import('../views/applicant/ReferralReceived.vue')
  },
  {
    path: '/applicant/status',
    component: () => import('../views/applicant/Status.vue')
  },
  {
    path: '/admin/overview',
    component: () => import('../views/administration/Overview.vue')
  },
  {
    path: '/admin/detail',
    component: () => import('../views/administration/Detail.vue')
  },
  {
    path: '/admin/courses',
    component: () => import('../views/administration/Courses.vue')
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  //base: 'index.html',
  routes
})

export default router
